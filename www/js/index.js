//var urlService = 'http://ve-trading.pe/index.php/service/';
var urlService = 'http://runningbox.azurewebsites.net/Home/';
var data = {};
var map,url,width;
var bar = new ldBar("#progress-bar");
bar.set(0);
var imagen="";
var archivo="";

var app = {
    initialize: function() {
      document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
      //this.onDeviceReady();
    },
    onDeviceReady: function() {
      this.receivedEvent('deviceready');
    },
    receivedEvent: function(id) {
      map = new MapManager();

      setTimeout(function(){
        width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) - 20;
        $('#pic').attr('width',width).attr('height',width*(1.8/3));
      },100);

      url = urlService+'ListaEvento';
      request.get(url,function(events){
        for(var i = 0; i < events.length; i++){
          var option = document.createElement('option');
          $(option).data('tipo',parseInt(events[i]['TIPO']));
          $(option).attr('id',events[i]['EVENTO'].replace(/ /g,''));
          $(option).val(events[i]['EVENTO']);
          $(option).html(events[i]['EVENTO']);
          $('#event').append(option);
        }
      });

      $('#event').change(function(){
        data.event = $(this).val();
        if(data.event !== "0") {
          $('#event').addClass('filled');
          data.tipo = $('#'+data.event.replace(/ /g,'')).data('tipo');
        }
        else {
          $('#event').removeClass('filled');
          data.tipo = undefined;
        }
      });

      $('#doc').change(function(){
        data.doc = $(this).val();
        if(data.doc !== "0")
          $('#doc').addClass('filled');
        else
          $('#doc').removeClass('filled');
      });

      $('#getpos').click(function(){
        var btn = this;
        $(btn).attr('disabled',true);
        map.updatePos(function(){
          $(btn).removeAttr('disabled');
          $('#lat').addClass('filled').html(map.lat);
          $('#lng').addClass('filled').html(map.lng);
          data.lat = map.lat;
          data.lng = map.lng;
        },function(){
          $(btn).removeAttr('disabled');
        });
      });

      $('#readcode').click(function(){
        var btn = this;
        $(btn).attr('disabled',true);
        $('#code').attr('disable',true);
        qreader.scan(
          function(code){
            $(btn).removeAttr('disabled');
            $('#code').removeAttr('disable');
            $('#code').val(code);
            /*$('#code').addClass('filled').html(code);
            data.code = code;*/
          },
          function(){
            $(btn).removeAttr('disabled');
            $('#code').removeAttr('disable');
          }
        );
      });

      $('#readdni').click(function(){
        var btn = this;
        $(btn).attr('disabled',true);
        $('#code_doc').attr('disable',true);
        qreader.scan(function(code){
          $(btn).removeAttr('disabled');
          $('#code_doc').removeAttr('disable');
          $('#code_doc').val(code);
          /*$('#code_doc').addClass('filled').html(code);
          data.code_doc = code;*/
        },function(){
          $(btn).removeAttr('disabled');
          $('#code_doc').removeAttr('disable');
        });
      });

       function getBlob(b64Data, contentType, sliceSize = 512) {
      contentType = contentType || '';
      sliceSize = sliceSize || 512;

      let byteCharacters = atob(b64Data);
      let byteArrays = [];

      for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        let slice = byteCharacters.slice(offset, offset + sliceSize);

        let byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        let byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
      }

      let blob = new Blob(byteArrays, {
        type: contentType
      });
      return blob;
    }

    function uploadFile(imageData) {

        //var fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
        //var fileName = new Date().getTime() + ".jpg";

        var tiempo = new Date().getTime();


        var fileName =  $('#code').val();
        fileName = fileName + "_" + tiempo + ".jpg";

       
        alert(fileName);

       archivo = fileName;


      var storageurl = "https://mirandafiles.blob.core.windows.net/running/" + fileName +
        "?st=2017-12-05T09%3A05%3A00Z&se=2030-12-06T09%3A05%3A00Z&sp=rwl&sv=2017-04-17&sr=c&sig=Rdx46kvWid5hBb2PO9Q81WhOwrzzHWNQaqE9nnvBhVk%3D";

      var data = getBlob(imageData, 'image/jpeg');

      $.ajax({
        type: 'PUT',
        url: storageurl,
        data: data,
        processData: false,
        contentType: false,
        headers: {
          "x-ms-blob-type": "BlockBlob",
          "x-ms-blob-content-type": "image/jpeg"
        },
        error: function (xhr, ajaxOptions, thrownError) {
          bar.set(0);
          alert(xhr.responseText);
        },
        xhr: function () {
          var xhr = new window.XMLHttpRequest();
          //Download progress
          xhr.upload.addEventListener("progress", function (evt) {
            if (evt.lengthComputable) {
              var percentComplete = Math.round((evt.loaded * 100) / evt.total);
              console.log(percentComplete);
              bar.set(percentComplete);
            }
          }, false);
          return xhr;
        },
        beforeSend: function () {
          bar.set(0);
        },
        success: function (json) {
          bar.set(100);
          //alert("Foto subida exitosamente");
          console.log(storageurl);
        }
      });
    }
    $('#btnqui').click(function () {
        imagen="";
        $('#pic').attr('src', "img/default-img.gif");
        document.getElementById("btnqui").style.display = "none";
    });

    $('#takepic').click(function () {
      var btn = this;
      $(btn).attr('disabled', true);
      camera.take(function (fileData) {

        $('#pic').attr('src', "data:image/jpeg;base64," + fileData);
        $(btn).removeAttr('disabled');
        document.getElementById("btnqui").style.display = "block";
        imagen = fileData;

        //uploadFile(fileData);


      }, function () {
        $(btn).removeAttr('disabled');
      });
    });

      $('#save').click(function(){



        var btn = this;
        $(btn).attr('disabled',true);
        data.name = $('#name').val();
        data.code = $('#code').val();
        data.obs = $('#obs').val();
        data.code_doc = $('#code_doc').val();
        if(!data.event || data.event == "0" || data.code.length === 0)
        {
          $(btn).removeAttr('disabled');
          alert('Debe registrar la orden y seleccionar un evento');
          return;
        }
        if((!data.doc || data.doc == "0" || data.code_doc.length === 0 || data.name.length === 0) && data.tipo === 1) {
          $(btn).removeAttr('disabled');
          alert('Ingrese los datos del merchant');
          return;
        }

       if(imagen==""){
             archivo="";
          }else{
             uploadFile(imagen);

          }


        //alert(archivo);

        var ws_data = {
          id         : data.code,
          des_cpoint : data.event,
          latitud    : data.lat,
          altitud    : data.lng,
          nrodoc     : data.code_doc,
          documento  : data.doc,
          nombre     : data.name,
          observacion : data.obs,
          imagen     : archivo
        };
        var params = $.param(ws_data);
        url = urlService+'Build1';
        request.post(url,params,
          function(resp){
            $(btn).removeAttr('disabled');
            if(resp === "1") {

              $('#pic').attr('src', "data:image/jpeg;base64," + imagen);
              data = {};
              alert('Se guardo correctamente la orden');
              document.getElementById("btnqui").style.display = "none";
              $('#event').removeClass('filled').val(0);
              $('#code').val('');
              $('#name').val('');
              $('#doc').removeClass('filled').val(0);
              $('#code_doc').val('');
              $('#lat').removeClass('filled').html('Latitud');
              $('#lng').removeClass('filled').html('Longitud');
              $('#pic').attr('src','img/default-img.gif');
              $('#obs').val('');
            }
            else {
              alert(resp);
            }
          },
          function() {
            $(btn).removeAttr('disabled');
            alert('Hay un problema de conexión con el servidor');
          }
        );

      });
    }
};

app.initialize();


