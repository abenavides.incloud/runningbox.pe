var qreader = {
  scan: function(callback,fail) {
    cordova.plugins.barcodeScanner.scan(
      function (result) {
        if(!result.cancelled) {
          callback(result.text);
          /*if(result.format === 'BAR_CODE')
            callback(result.text);
          else
            alert('No ha escaneado un código de barras');*/
        }
        else {
          fail();
        }
      },
      function (error) {
        //alert('Se necesita permiso para acceder a la cámara');
        fail();
        alert("Error al escanear: " + error);
      }
    );
  }
};
