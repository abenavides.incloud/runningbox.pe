function MapManager() {
  this.geocoder = new google.maps.Geocoder();
  this.lat = null;
  this.lng = null;
  this.location = null;
  this.address = null;
}
MapManager.prototype.updatePos = function(callback,fail) {
  var map = this;
  navigator.geolocation.getCurrentPosition(
    function(position) {
      map.lat = position.coords.latitude;
      map.lng = position.coords.longitude;
      map.location = new google.maps.LatLng(map.lat,map.lng);

      map.geocoder.geocode({ 'latLng': map.location }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          map.address = results[0].formatted_address;
        }
        callback();
      });
    },
    function() {
      fail();
      alert('Activa el GPS');
    },
    {
      enableHighAccuracy: true,
      timeout: 5000,
    }
  );
}
