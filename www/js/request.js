var request = {
  callback: function(url, data, callback, fail, response)
  {
    $('.loading').hide();
    if(typeof callback === 'function') {
      callback(response);
    }
  },
  //XMLHttpRequest Get
  get: function(url, callback, fail) {
    $('.loading').show();
    $.get(url)
      .done(function(response) {
        request.callback(url,null,callback,fail,response);
      })
      .fail(function() {
        $('.loading').hide();
        if(typeof fail === 'function') {
          fail();
        }
      });
  },
  //XMLHttpRequest Post
  post: function(url, data, callback, fail) {
    $('.loading').show();
    $.post(url,data)
      .done(function(response) {
        request.callback(url,data,callback,fail,response);
      })
      .fail(function() {
        $('.loading').hide();
        if(typeof fail === 'function') {
          fail();
        }
      });
  }
}
