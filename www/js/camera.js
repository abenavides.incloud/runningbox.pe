var camera = {
  take: function(callback,fail) {
    navigator.camera.getPicture(
      function(imgData) {
        callback(imgData);
      },
      function(error) {
        fail();
        if(error !== 'Camera cancelled.')
          alert('Error: ' + error);
      },
      {
        destinationType: navigator.camera.DestinationType.DATA_URL,
        sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
        quality: 80,
        targetWidth: 1500,
        targetHeight: 800
      }
    );
  }
}
